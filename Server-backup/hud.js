﻿var g_menu = API.createMenu("Fraktion auswählen", "Wähle deine Fraktion aus.", 0, 0, 6);
g_menu.ResetKey(menuControl.Back);
g_menu.AddItem(API.createMenuItem("Criminal", "Du bist Criminal auf diesem Server"));
g_menu.AddItem(API.createMenuItem("Cop", "Du bist Cop auf diesem Server"));

var goto_menu = API.createMenu("Teleport", "Wähle dein Ort", 0, 0, 6);
goto_menu.ResetKey(menuControl.Back);
goto_menu.AddItem(API.createMenuItem("Criminal-Spawn", ""));
goto_menu.AddItem(API.createMenuItem("Police-Spawn", ""));


goto_menu.OnItemSelect.connect(function (sender, item, index) {
    API.sendChatMessage("Du bist nun am ~g~" + item.Text);
    API.triggerServerEvent("SELECT_ADM_GOTO", item.Text);
    API.showCursor(false);
    goto_menu.Visible = false;
});


g_menu.OnItemSelect.connect(function(sender, item, index) {
    API.sendChatMessage("You selected: ~g~" + item.Text);
    API.triggerServerEvent("SELECT_FRAKTION", item.Text);
	API.showCursor(false);
	g_menu.Visible = false;
});

API.onServerEventTrigger.connect(function(name, args) {
	if (name == "OPEN_LOGIN_MENU") {
		API.showCursor(true);
		g_menu.Visible = true;
	}
	if (name == "OPEN_ADM_GOTO") {
	    API.showCursor(true);
	    goto_menu.Visible = true;
	}
});

API.onUpdate.connect(function() {
    API.drawMenu(g_menu);
    API.drawMenu(goto_menu);
});
