﻿using System;
using GTANetworkServer;
using GTANetworkShared;

namespace Server
{
    public class Functions : Script
    {
        public void ProxDetector(float radius, Client player, string message, string col1, string col2, string col3, string col4, string col5)
        {
            var players = API.getPlayersInRadiusOfPlayer(radius, player);
            foreach (Client c in players)
            {
                if (player.position.DistanceTo(c.position) <= radius / 16)
                {
                    API.sendChatMessageToPlayer(c, col1, message);
                }
                else if (player.position.DistanceTo(c.position) <= radius / 8)
                {
                    API.sendChatMessageToPlayer(c, col2, message);
                }
                else if (player.position.DistanceTo(c.position) <= radius / 4)
                {
                    API.sendChatMessageToPlayer(c, col3, message);
                }
                else if (player.position.DistanceTo(c.position) <= radius / 2)
                {
                    API.sendChatMessageToPlayer(c, col4, message);
                }
                else if (player.position.DistanceTo(c.position) <= radius)
                {
                    API.sendChatMessageToPlayer(c, col5, message);
                }
            }
        }
    }
}
