﻿using System;
using GTANetworkServer;
using GTANetworkShared;

namespace Server
{
    public class AdminCmds : Script
    {

        [Command("goto")]
        public void GotoMenu(Client player)
        {
            if (API.getEntityData(player, "Adminlevel") >= 1)
            {
                API.triggerClientEvent(player, "OPEN_ADM_GOTO");
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Du bist kein Admin");
            }
        }

        [Command("veh")]
        public void spawnVehicle(Client player, string veh, int col1, int col2)
        {
            if(API.getEntityData(player, "Adminlevel") >= 1)
            {
                Vector3 vehPos = API.getEntityPosition(player);
                Vector3 vehRot = API.getEntityRotation(player);
                VehicleHash myVehicle = API.vehicleNameToModel(veh);
                API.sendChatMessageToPlayer(player, "Du hast dir ein " + myVehicle + " gespawnt.");
                var vehID = API.createVehicle(myVehicle, vehPos, vehRot, col1, col2); //Spawned vehicle is visible in all Dimensions
                API.setPlayerIntoVehicle(player, vehID, -1);
            }
            else  
            {
                API.sendChatMessageToPlayer(player, "Du bist kein Admin");
            }
        }
        [Command("getpos")]
        public void GetPosition(Client player, string text)
        {
            if (API.getEntityData(player, "Adminlevel") >= 1)
            {
                Vector3 PlayerPos = API.getEntityPosition(player);
                string PosX = PlayerPos.X.ToString("0.000");
                string PosY = PlayerPos.Y.ToString("0.000");
                string PosZ = PlayerPos.Z.ToString("0.000");
                API.sendChatMessageToPlayer(player, "X: " + PlayerPos.X + " Y: " + PlayerPos.Y + " Z: " + PlayerPos.Z + " || Anmerkung: " + text);
                API.consoleOutput(player.name + ": " + PosX.Replace(",", ".") + "," + PosY.Replace(",", ".") + "," + PosZ.Replace(",", ".") + " || Anmerkung: " + text);
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Du bist kein Admin");
            }
        }
        [Command("freeze")]
        public void freezePlayer(Client player, string playerId)
        {
            Client targetPlayer = API.exported.playerids.findPlayer(player, playerId);
            targetPlayer.freezePosition = true;
            API.sendChatMessageToPlayer(player, "Player "+ targetPlayer.name + " gefreezed.");
        }
        [Command("unfreeze")]
        public void unfreezePlayer(Client player, string playerId)
        {
            Client targetPlayer = API.exported.playerids.findPlayer(player, playerId);
            targetPlayer.freezePosition = false;
            API.sendChatMessageToPlayer(player, "Player " + targetPlayer.name + " unfreezed.");
        }
        [Command("menutest")]
        public void menuTest(Client player)
        {
            API.triggerClientEvent(player, "OPEN_LOGIN_MENU");
            API.sendChatMessageToPlayer(player, "Menutest");
        }
        [Command("resetarrest")]
        public void gethp(Client player, Client target)
        {
            API.setEntityData(target, "arrested", false);
        }
    }
}
