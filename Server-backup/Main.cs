﻿using System;
using GTANetworkServer;
using GTANetworkShared;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace Server
{
    public class Main : Script
    {

        public Timer RangeRadar;
        private Client playera;

        public Main()
        {
            API.onUpdate += OnUpdateHandler;
            API.onResourceStart += OnResourceStart;
            API.onResourceStop += OnResourceStop;
            API.onChatMessage += OnSentMessage;
            API.onPlayerConnected += OnPlayerConnected;
            API.onPlayerDisconnected += OnPlayerDisconnected;
            API.onPlayerEnterVehicle += OnPlayerEnterVehicle;
            API.onPlayerExitVehicle += OnPlayerExitVehicleHandler;
            API.onPlayerFinishedDownload += OnPlayerFinishedDownloadHandler;
            API.onChatCommand += OnChatCommand;
            API.onClientEventTrigger += onClientEventTrigger;
        }
        public void onClientEventTrigger(Client sender, string name, object[] args)
        {
            if(name == "SELECT_ADM_GOTO")
            {
                string Selected = (string)args[0];
                switch(Selected)
                {
                    case "Criminal-Spawn":
                        API.setEntityPosition(sender, new Vector3(-643, -1228, 11.54758));
                        break;

                    case "Police-Spawn":
                        API.setEntityPosition(sender, new Vector3(458, -990, 30.6896));
                        break;

                    default:
                        API.kickPlayer(sender);
                        break;
                }
            }
            if (name == "SELECT_FRAKTION")
            {
                string Fraktion = (string)args[0];
                if(Fraktion == "Cop")
                {
                    //copspawn & default cop skin
                    Vector3 CopSpawn = new Vector3(458.4821, -990.9999, 30.6896);
                    string DefaultCopSkin = "CopCutscene";

                    API.setEntityData(sender, "fraction", 1);
                    API.setEntityPosition(sender.handle, CopSpawn);
                    API.setPlayerSkin(sender, API.pedNameToModel(DefaultCopSkin));
                    sender.invincible = false;

                    return;
                }
                else if(Fraktion == "Criminal")
                {
                    //criminalspawn & default criminal skin
                    Vector3 CriminalSpawn = new Vector3(-643.0175, -1228.023, 11.54758);
                    string DefaultCriminalSkin = "SouCent04AMM";

                    API.setEntityData(sender, "fraction", 2);
                    API.setEntityPosition(sender.handle, CriminalSpawn);
                    API.setPlayerSkin(sender, API.pedNameToModel(DefaultCriminalSkin));
                    sender.invincible = false;

                    return;
                }
                else
                {
                    API.kickPlayer(sender);

                    return;
                }
            }
        }

        public void OnUpdateHandler()
        {
            //
        }
        public void OnPlayerFinishedDownloadHandler(Client player)
        {
            login(player);

            //money
            var money = User.getmoney(player);
            player.setSyncedData("money", money);
            API.triggerClientEvent(player, "update_money_display", money);
        }
        public void OnPlayerConnected(Client player)
        {
            API.sendChatMessageToAll(player.name +" hat den Server betreten.");
            API.consoleOutput("[Connect]" + player.name + " hat den Server betreten.");
            //player.freezePosition = true;
            player.invincible = true;
            API.setEntityData(player, "Adminlevel", 5);

            API.setEntityData(player, "fraction", 0);
            playera = player;

        }
        public void OnPlayerDisconnected(Client player, string reason)
        {
            API.sendChatMessageToAll(player.name + " hat den Server verlassen.");
            API.consoleOutput("[Disconnected]" + player.name + " hat den Server verlassen[" + reason + "]");
        }
        public void OnResourceStart()
        {
            API.consoleOutput("Der Server wurde gestartet");

            RangeRadar = new Timer(1000);
            RangeRadar.Elapsed += new ElapsedEventHandler(SearchingForCriminalshandler);
            RangeRadar.Start();

            //npc an den Schreibtisch stellen
            Vector3 npcpos = new Vector3(441.2433, -978.4735, 30.6896);
            string npcskin = "CopCutscene";
            API.createPed(API.pedNameToModel(npcskin), npcpos, 180, 0);
        }

        public void SearchingForCriminalshandler(object sender, ElapsedEventArgs e)
        {
            if (API.getEntityData(playera, "fraction") == 1 & API.isPlayerInAnyVehicle(playera) == true)
            {
                var instance = new RangeRadarSearchForCriminals();
                instance.StartSearchingForCriminals(playera);
            }
            else
            {
                return;
            }
        }

        public void OnResourceStop()
        {
            API.consoleOutput("Der Server wurde gestopt");
        }
        public void OnSentMessage(Client player, string message, CancelEventArgs e)
        {
            Functions func = new Functions();
            func.ProxDetector(30, player, player.name.Replace("_", " ") + " sagt: " + message, "~#FFFFFF~", "~#C8C8C8~", "~#AAAAAA~", "~#8C8C8C~", "~#6E6E6E~");
            API.consoleOutput(player.name.Replace("_", " ") + " sagt: " + message);
            e.Cancel = true;
            return;
        }
        public void OnChatCommand(Client player, string command, CancelEventArgs e)
        {
            API.sendChatMessageToPlayer(player, "Command: " + command);
        }


        public void login(Client player)
        {
            API.triggerClientEvent(player, "OPEN_LOGIN_MENU");
        }

        public void OnPlayerExitVehicleHandler(Client player, NetHandle vehicle)
        {
            var instance = new RangeRadarSearchForCriminals();
            instance.RangeRadarStop(player, vehicle);
        }

        public void OnPlayerEnterVehicle(Client player, NetHandle vehicle)
        {
            var instance = new RangeRadarSearchForCriminals();
            instance.RangeRadarStart(player, vehicle);
        }
    }
}

