﻿using GTANetworkServer;
using GTANetworkShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Server
{
    public class RangeRadarSearchForCriminals : Script
    {

        //Radar timer & vars ...start
        private Timer Arrest;
        private Client playera;
        private Client playerb;
        private Client targetb;
        //Radar timer & vars ...ende

        public void RangeRadarStart(Client player, NetHandle Vehicle)
        {
            //Cops initialisieren
            List<string> Cops = new List<string>();

            var isplayercop = API.getEntityData(player, "fraction");

            if (isplayercop == 1)
            {
                var IsPlayerInList = Cops.Contains(player.name);
                
                if (IsPlayerInList != true)
                {
                    Cops.Add(player.name);
                }
                API.sendNotificationToPlayer(player, "~g~Range-Radar started, youre searching for criminals!");
                playera = player;
            }
            else
            {
                return;
            }
        }

        public void StartSearchingForCriminals(Client playera)
        {
            List<Client> PlayersInRange = API.getPlayersInRadiusOfPlayer(6, playera);
            //criminals initialisieren
            List<string> Criminals = new List<string>();

            foreach (Client target in PlayersInRange)
            {
                if (API.getEntityData(target, "fraction") == 2 & API.getEntityData(target, "arrested") != true)
                {
                    API.sendChatMessageToPlayer(playera, "Du bist in der naehe eines Kriminellen, bleibe 5 Sekunden dran! ");
                    API.sendChatMessageToPlayer(target, "Ein Cop befindet sich in deiner Naehe, verschwinde! Du hast 5 Sekunden Zeit!");

                    Arrest = new Timer(5000);
                    Arrest.Elapsed += new ElapsedEventHandler(ArrestCriminal);
                    playerb = playera;
                    targetb = target;
                    Arrest.Start();
                }
            }
        }

        private void ArrestCriminal(object source, ElapsedEventArgs e)
        {
            List<Client> PlayersInRange = API.getPlayersInRadiusOfPlayer(6, playerb);
            var exists = PlayersInRange.Contains(targetb);
            if (exists == false)
            {
                Arrest.Stop();
                API.sendChatMessageToPlayer(playerb, "Verhaftung fehlgeschlagen!");
                API.sendChatMessageToPlayer(targetb, "Du bist entkommen!");
            }
            else if (API.getEntityData(targetb, "arrested") == true)
            {
                Arrest.Stop();
                API.sendChatMessageToPlayer(playerb, "Spieler wurde bereits gefasst!");
            }
            else
            {
                Arrest.Stop();
                var vehicle = API.getPlayerVehicle(playerb);
                API.setPlayerIntoVehicle(targetb, vehicle, 1);
                API.setEntityData(targetb, "arrested", true);
                API.sendChatMessageToPlayer(playerb, "Verhaftung erfolgreich!");
                API.sendChatMessageToPlayer(targetb, "Du wurdest verhaftet!");

                var money = playerb.getSyncedData("money");
                money = money + 50;
                playerb.setSyncedData("money", money);
                API.triggerClientEvent(playerb, "update_money_display", money);
                API.sendNotificationToPlayer(playerb, "~h~ ~u~ + ~g~50$");
            }
        }

        public void RangeRadarStop (Client player, NetHandle vehicle)
        {
            API.sendChatMessageToPlayer(player, "Radar ausgeschaltet, du kannst niemanden zu Fuß arresten!");
            return;
        }

    }
}
