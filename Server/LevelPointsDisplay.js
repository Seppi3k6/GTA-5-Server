﻿var currentPoints = null;
var resX = API.getScreenResolutionMantainRatio().Width;

API.onServerEventTrigger.connect(function (name, args) {
    if (name == "update_LevelPoints_display") {
        currentPoints = args[0];
        nextlevelPoints = args[1];
        currentLevel = args[2];
    }
});

API.onUpdate.connect(function () {
    if (currentPoints != null) {
        API.drawText("Level:" + currentLevel + "\n" + currentPoints + "/" + nextlevelPoints, resX - 15, 120, 1, 115, 186, 131, 255, 4, 2, false, true, 0);
    }
});