﻿using System;
using System.Data;
using GTANetworkServer;
using GTANetworkShared;
using System.Collections.Generic;

namespace Server
{
    class ResetUserData : Script
    {
        public ResetUserData()
        {
            API.onPlayerConnected += OnPlayerConnected;
            API.onPlayerDisconnected += OnPlayerDisconnected;
        }
        public void OnPlayerConnected(Client player)
        {
            API.setEntityData(player, "Level", 1);
            API.setEntityData(player, "Points", 0);
            API.setEntityData(player, "Adminlevel", 0);
            API.setEntityData(player, "Bigears", 0);
            API.setEntityData(player, "fraction", 0);
            API.setEntityData(player, "StartSearchingForCriminals", false);
        }
        public void OnPlayerDisconnected(Client player, string reason)
        {
            API.setEntityData(player, "Level", 1);
            API.setEntityData(player, "Points", 0);
            API.setEntityData(player, "Adminlevel", 0);
            API.setEntityData(player, "Bigears", 0);
            API.setEntityData(player, "fraction", 0);
            API.setEntityData(player, "StartSearchingForCriminals", false);
        }
    }
}
