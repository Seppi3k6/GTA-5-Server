﻿var g_menu = API.createMenu("Fraktion auswählen", "Wähle deine Fraktion aus.", 0, 0, 6);
g_menu.ResetKey(menuControl.Back);
g_menu.AddItem(API.createMenuItem("Criminal", "Du bist Criminal auf diesem Server"));
g_menu.AddItem(API.createMenuItem("Cop", "Du bist Cop auf diesem Server"));

var goto_menu = API.createMenu("Teleport", "Wähle dein Ort", 0, 0, 6);
goto_menu.ResetKey(menuControl.Back);
goto_menu.AddItem(API.createMenuItem("Criminal-Spawn", ""));
goto_menu.AddItem(API.createMenuItem("Police-Spawn", ""));

var copselector_menu = API.createMenu("Copcar Selector", "Choose your Car", 0, 0, 6);
copselector_menu.ResetKey(menuControl.Back);
copselector_menu.AddItem(API.createMenuItem("FBI", ""));
copselector_menu.AddItem(API.createMenuItem("Police", ""));
copselector_menu.AddItem(API.createMenuItem("SultanRS", ""));

var criminalselector_menu = API.createMenu("Car Selector", "Choose your Car", 0, 0, 6);
criminalselector_menu.ResetKey(menuControl.Back);
criminalselector_menu.AddItem(API.createMenuItem("Stanier", "Lvl 1"));
criminalselector_menu.AddItem(API.createMenuItem("Sultan", "lvl 3"));
criminalselector_menu.AddItem(API.createMenuItem("Elegy", "lvl 7"));


goto_menu.OnItemSelect.connect(function (sender, item, index) {
    API.sendChatMessage("Du bist nun am ~g~" + item.Text);
    API.triggerServerEvent("SELECT_ADM_GOTO", item.Text);
    API.showCursor(false);
    goto_menu.Visible = false;
});


g_menu.OnItemSelect.connect(function(sender, item, index) {
    API.sendChatMessage("You selected: ~g~" + item.Text);
    API.triggerServerEvent("SELECT_FRAKTION", item.Text);
	API.showCursor(false);
	g_menu.Visible = false;
});

copselector_menu.OnItemSelect.connect(function (sender, item, index) {
    API.sendChatMessage("You selected ~g~" + item.Text);
    API.triggerServerEvent("SELECT_COPCAR", item.Text);
    API.showCursor(false);
    copselector_menu.Visible = false;
});

criminalselector_menu.OnItemSelect.connect(function (sender, item, index) {
    API.sendChatMessage("You selected ~g~" + item.Text);
    API.triggerServerEvent("SELECT_CRIMINALCAR", item.Text);
    API.showCursor(false);
    criminalselector_menu.Visible = false;
});

API.onServerEventTrigger.connect(function(name, args) {
	if (name == "OPEN_LOGIN_MENU") {
		API.showCursor(true);
		g_menu.Visible = true;
	}
	if (name == "OPEN_ADM_GOTO") {
	    API.showCursor(true);
	    goto_menu.Visible = true;
	}
	if (name == "OPEN_SELECTCOPCAR_MENU") {
	    API.showCursor(true);
	    copselector_menu.Visible = true;
	}
	if (name == "OPEN_SELECTCRIMINALCAR_MENU") {
	    API.showCursor(true);
	    criminalselector_menu.Visible = true;
	}
});

API.onUpdate.connect(function() {
    API.drawMenu(g_menu);
    API.drawMenu(goto_menu);
    API.drawMenu(copselector_menu);
    API.drawMenu(criminalselector_menu);
});
