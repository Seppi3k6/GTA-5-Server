﻿

var currentprogress = 0;
var Browser = null;
var resX = API.getScreenResolutionMantainRatio().Width;
resX = Math.ceil(resX / 2);

API.onServerEventTrigger.connect(function (name, args) {
    if (name == "update_progress_display") {
        currentprogress = args[0];
    }
});

API.onUpdate.connect(function () {
    if (currentprogress != 0) {
        if (Browser == null) {
            Browser = API.createCefBrowser(550, 80); //we're initializing the browser here. This will be the full size of the user's screen.
            API.waitUntilCefBrowserInit(Browser); //this stops the script from getting ahead of itself, it essentially pauses until the browser is initialized
            API.setCefBrowserPosition(Browser, resX - 250, 25); //The orientation (top left) corner in relation to the user's screen.  This is useful if you do not want a full page browser.  0,0 is will lock the top left corner of the browser to the top left of the screen.   .    
            //API.setCefBrowserHeadless(Browser, true);
            API.loadPageCefBrowser(Browser, "progressbar.html");
        }
        Browser.call("move", currentprogress);
    }
    else if (Browser != null)
    {
        API.destroyCefBrowser(Browser);
        Browser = null;
    }
});