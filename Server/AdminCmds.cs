﻿using System;
using GTANetworkServer;
using GTANetworkShared;

namespace Server
{
    public class AdminCmds : Script
    {

        [Command("goto")]
        public void GotoMenu(Client player)
        {
            if (API.getEntityData(player, "Adminlevel") >= 1)
            {
                API.triggerClientEvent(player, "OPEN_ADM_GOTO");
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Du bist kein Admin");
            }
        }

        [Command("veh")]
        public void spawnVehicle(Client player, string veh, int col1, int col2)
        {
            if(API.getEntityData(player, "Adminlevel") >= 1)
            {
                Vector3 vehPos = API.getEntityPosition(player);
                Vector3 vehRot = API.getEntityRotation(player);
                VehicleHash myVehicle = API.vehicleNameToModel(veh);
                API.sendChatMessageToPlayer(player, "Du hast dir ein " + myVehicle + " gespawnt.");
                var vehID = API.createVehicle(myVehicle, vehPos, vehRot, col1, col2); //Spawned vehicle is visible in all Dimensions
                API.setPlayerIntoVehicle(player, vehID, -1);
            }
            else  
            {
                API.sendChatMessageToPlayer(player, "Du bist kein Admin");
            }
        }
        [Command("getpos")]
        public void GetPosition(Client player, string text)
        {
            if (API.getEntityData(player, "Adminlevel") >= 1)
            {
                Vector3 PlayerPos = API.getEntityPosition(player);
                string PosX = PlayerPos.X.ToString("0.00000");
                string PosY = PlayerPos.Y.ToString("0.00000");
                string PosZ = PlayerPos.Z.ToString("0.00000");

                Vector3 PlayerRot = API.getEntityRotation(player);
                API.sendChatMessageToPlayer(player, "X: " + PlayerPos.X + " Y: " + PlayerPos.Y + " Z: " + PlayerPos.Z + " Rot: " + PlayerRot + " || Anmerkung: " + text);
                API.consoleOutput(player.name + ": " + PosX.Replace(",", ".") + "," + PosY.Replace(",", ".") + "," + PosZ.Replace(",", ".") + " Rot: " + PlayerRot + " || Anmerkung: " + text);
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Du bist kein Admin");
            }
        }
        [Command("freeze")]
        public void freezePlayer(Client player, string playerId)
        {
            Client targetPlayer = API.exported.playerids.findPlayer(player, playerId);
            targetPlayer.freezePosition = true;
            API.sendChatMessageToPlayer(player, "Player "+ targetPlayer.name + " gefreezed.");
        }
        [Command("unfreeze")]
        public void unfreezePlayer(Client player, string playerId)
        {
            Client targetPlayer = API.exported.playerids.findPlayer(player, playerId);
            targetPlayer.freezePosition = false;
            API.sendChatMessageToPlayer(player, "Player " + targetPlayer.name + " unfreezed.");
        }
        [Command("menutest")]
        public void menuTest(Client player)
        {
            API.triggerClientEvent(player, "OPEN_LOGIN_MENU");
            API.sendChatMessageToPlayer(player, "Menutest");
        }
        [Command("resetarrest")]
        public void resetarrest(Client player, string playerId)
        {
            Client targetPlayer = API.exported.playerids.findPlayer(player, playerId);
            API.setEntityData(targetPlayer, "arrested", false);
        }
        [Command("sethp")]
        public void setPlayerHealth(Client player, string playerId, int health)
        {
            if (API.getEntityData(player, "Adminlevel") >= 1)
            {
                Client targetPlayer = API.exported.playerids.findPlayer(player, playerId);
                API.setPlayerHealth(targetPlayer, health);
                API.sendChatMessageToPlayer(player, "Du hast " + targetPlayer.name +" auf " + health + " gesetzt.");
                API.sendChatMessageToPlayer(targetPlayer, player.name + " hat dich auf " + health + " gesetzt.");
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Du bist kein Admin");
            }
        }
        [Command("setadminlvl")]
        public void setadminlvl(Client player, string playerId, int lvl)
        {
            if (API.getEntityData(player, "Adminlevel") >= 5)
            {
                Client targetPlayer = API.exported.playerids.findPlayer(player, playerId);
                API.setEntityData(targetPlayer, "Adminlevel", lvl);
                API.sendChatMessageToPlayer(player, "~b~Did set " + targetPlayer.name + " to Adminrank " + lvl + "!");
                API.sendChatMessageToPlayer(targetPlayer, "~b~FullAdmin " + player.name + " set you to Adminrank " + lvl + "!");
                return;
            }
            else
            {
                API.sendChatMessageToPlayer(player, "~r~Youre not a FullAdmin!");
                return;
            }
        }
        [Command("tp")]
        public void TeleportPlayerToPlayerCommand(Client player, string playerId)
        {
            if (API.getEntityData(player, "Adminlevel") >= 1)
            {
                Client targetPlayer = API.exported.playerids.findPlayer(player, playerId);
                API.setEntityPosition(player.handle, API.getEntityPosition(targetPlayer.handle));
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Du bist kein Admin");
            }
        }
        [Command("restartres")]
        public void restartres(Client player, string resourcename)
        {
            if (API.getEntityData(player, "Adminlevel") >= 5)
            {
                API.stopResource(resourcename);
                API.startResource(resourcename);
                return;
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Du besitzt nicht die dafür benötigten Rechte!");
                return;
            }
        }
        [Command("bigears")]
        public void ToggleBigears(Client player)
        {
            var instance = new UserFunctions();
            if(instance.GetPlayerAdminlevel(player) >= 1)
            {
                instance.TogglePlayerBigears(player);
                return;
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Du besitzt nicht die dafür benötigten Rechte!");
                return;
            }
        }
        [Command("clearplayerwanteds", Alias = "USAGE: /clearplayerwanteds [NAME] [ANZAHL]")]
        public void ClearPlayerWanteds(Client player, string playerId, int wanteds)
        {
            var instance = new UserFunctions();
            if (instance.GetPlayerAdminlevel(player) >= 1)
            {
                Client targetPlayer = API.exported.playerids.findPlayer(player, playerId);
                var wantedsys = new Wanted_System();
                wantedsys.ClearPlayerWanteds(targetPlayer, wanteds);
                API.sendChatMessageToPlayer(player, "~y~Du hast " + targetPlayer.name + " " + wanteds + " wanteds entfernt, er hat nun " + wantedsys.GetPlayerWanteds(targetPlayer) + " wanteds.");
                API.sendChatMessageToPlayer(targetPlayer, "~y~Admin " + player.name + " hat dir " + wanteds + " wanteds entfernt, du hast nun " + wantedsys.GetPlayerWanteds(targetPlayer) + " wanteds.");
                return;
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Du besitzt nicht die dafür benötigten Rechte!");
                return;
            }
        }
        [Command("giveplayerwanteds", Alias = "USAGE: /giveplayerwanteds [NAME] [ANZAHL]")]
        public void GivePlayerWanteds(Client player, string playerId, int wanteds)
        {
            var instance = new UserFunctions();
            if (instance.GetPlayerAdminlevel(player) >= 1)
            {
                Client targetPlayer = API.exported.playerids.findPlayer(player, playerId);
                var wantedsys = new Wanted_System();
                wantedsys.GivePlayerWeanteds(targetPlayer, wanteds);
                API.sendChatMessageToPlayer(player, "~y~Du hast " + targetPlayer.name + " " + wanteds + " wanteds gegeben, er hat nun " + wantedsys.GetPlayerWanteds(targetPlayer) + " wanteds.");
                API.sendChatMessageToPlayer(targetPlayer, "~y~Admin " + player.name + " hat dir " + wanteds + " wanteds gegeben, du hast nun " + wantedsys.GetPlayerWanteds(targetPlayer) + " wanteds.");
                return;
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Du besitzt nicht die dafür benötigten Rechte!");
                return;
            }
        }
        [Command("testprogress")]
        public void testprogress(Client player, int progress)
        {
            API.triggerClientEvent(player, "update_progress_display", progress);
            API.sendChatMessageToPlayer(player, "testprogress gestartet");
        }
    }
}
