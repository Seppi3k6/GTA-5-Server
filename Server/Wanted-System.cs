﻿using System;
using System.Data;
using GTANetworkServer;
using GTANetworkShared;
using System.Collections.Generic;
using System.Timers;




namespace Server
{
    public class Wanted_System : Script
    {
        public Wanted_System()
        {
            API.onResourceStart += OnResourceStart;
            API.onPlayerConnected += OnPlayerConnected;
        }
        //Einstellungen________________________________________
        int wantedlostTime = 20;  //Sekunden
        //_____________________________________________________|

        public System.Timers.Timer WantedSystemTimer;

        public int GetPlayerWanteds(Client player)
        {
            return API.getEntityData(player, "wantedlevel");
        }
        public void SetPlayerWanteds(Client player, int newwanted)
        {
            API.setPlayerWantedLevel(player, newwanted);
            API.setEntityData(player, "wantedlevel", newwanted);
            API.setEntityData(player, "wantedlevelTime", wantedlostTime);
            return;
        }
        public void GivePlayerWeanteds(Client player, int newwanted)
        {
            int playerwanteds = API.getEntityData(player, "wantedlevel");
            playerwanteds += newwanted;
            API.setPlayerWantedLevel(player, playerwanteds);
            API.setEntityData(player, "wantedlevel", playerwanteds);
            API.sendChatMessageToPlayer(player, "~#E1FF00~","Wantedlevel: " + playerwanteds);
            API.setEntityData(player, "wantedlevelTime", wantedlostTime);

            var instance = new ChatMessageFunction();
            instance.SendCopMessage("~r~" + player.name + " Wantedlevel: " + GetPlayerWanteds(player));
            return;
        }
        public int ClearPlayerWanteds(Client player, int clearwanted)
        {
            int playerwanteds = API.getEntityData(player, "wantedlevel");
            playerwanteds -= clearwanted;
            API.setPlayerWantedLevel(player, playerwanteds);
            API.setEntityData(player, "wantedlevel", playerwanteds);
            API.sendChatMessageToPlayer(player, "~#E1FF00~","Wantedlevel: " + playerwanteds);
            return 1;
        }
        public void OnPlayerWantedsUpdate(object sender, ElapsedEventArgs e)
        {
            int playerwantedTime;
            List<Client> AllPlayers = API.getAllPlayers();
            foreach (Client player in AllPlayers)
            {
                if (API.getEntityData(player, "fraction") == 2)
                {
                    if (GetPlayerWanteds(player) >= 1)
                    {
                        playerwantedTime = API.getEntityData(player, "wantedlevelTime");
                        playerwantedTime --;
                        API.setEntityData(player, "wantedlevelTime", playerwantedTime);
                        if(playerwantedTime == 0)
                        {
                            ClearPlayerWanteds(player, 1);
                            API.setEntityData(player, "wantedlevelTime", wantedlostTime);
                        }
                    }
                }
            }
            return;
        }
        public void OnResourceStart()
        {
            WantedSystemTimer = new System.Timers.Timer(1000);
            WantedSystemTimer.Elapsed += new ElapsedEventHandler(OnPlayerWantedsUpdate);
            WantedSystemTimer.Start();
        }
        public void OnPlayerConnected(Client player)
        {
            API.setEntityData(player, "wantedlevelTime", wantedlostTime);
        }
    }
}
