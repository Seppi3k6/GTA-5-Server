﻿
        var Browser = null;

        API.onServerEventTrigger.connect(function (name, args) {
            if (name == "OPEN_LOGIN_SCREEN") {
                API.destroyCefBrowser(Browser);
                var res = API.getScreenResolution(); //this gets the client's screen resoulution
                Browser = API.createCefBrowser(res.Width, res.Height); //we're initializing the browser here. This will be the full size of the user's screen.
                API.waitUntilCefBrowserInit(Browser); //this stops the script from getting ahead of itself, it essentially pauses until the browser is initialized
                API.setCefBrowserPosition(Browser, 0, 0); //The orientation (top left) corner in relation to the user's screen.  This is useful if you do not want a full page browser.  0,0 is will lock the top left corner of the browser to the top left of the screen.
                API.loadPageCefBrowser(Browser, "loginscreen.html"); //This loads the HTML file of your choice.      .    
                //API.setCefBrowserHeadless(Browser, true); //this will remove the scroll bars from the bottom/right side
                API.showCursor(true); //This will show the mouse cursor
                API.setCanOpenChat(false);  //This disables the chat, so the user can type in a form without opening the chat and causing issues.
                
            }
        });

        API.onServerEventTrigger.connect(function (name, args) {
            if (name == "OPEN_REGISTER_SCREEN") {
                var res = API.getScreenResolution(); //this gets the client's screen resoulution
                Browser = API.createCefBrowser(res.Width, res.Height); //we're initializing the browser here. This will be the full size of the user's screen.
                API.waitUntilCefBrowserInit(Browser); //this stops the script from getting ahead of itself, it essentially pauses until the browser is initialized
                API.setCefBrowserPosition(Browser, 0, 0); //The orientation (top left) corner in relation to the user's screen.  This is useful if you do not want a full page browser.  0,0 is will lock the top left corner of the browser to the top left of the screen.
                API.loadPageCefBrowser(Browser, "registerscreen.html"); //This loads the HTML file of your choice.      .    
                //API.setCefBrowserHeadless(Browser, true); //this will remove the scroll bars from the bottom/right side
                API.showCursor(true); //This will show the mouse cursor
                API.setCanOpenChat(false);  //This disables the chat, so the user can type in a form without opening the chat and causing issues.
            }
        });

        function login(Password) 
        {
            API.triggerServerEvent("LOGIN", Password);
            API.showCursor(false); //stop showing the cursor
            API.destroyCefBrowser(Browser); //destroy the CEF browser
            API.setCanOpenChat(true); //allow the player to use the chat again.
        }

        function register(Password) {
            API.triggerServerEvent("REGISTER", Password);
            API.showCursor(false); //stop showing the cursor
            API.destroyCefBrowser(Browser); //destroy the CEF browser
            API.setCanOpenChat(true); //allow the player to use the chat again.
        }