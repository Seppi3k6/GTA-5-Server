﻿using System;
using System.Data;
using GTANetworkServer;
using GTANetworkShared;
using System.Collections.Generic;
using System.Timers;
using BCr = BCrypt.Net;

namespace Server
{
    class PrisonHandler : Script
    {
        //Einstellungen________________________________________
        int JailTime = 30;  //Sekunden
        //_____________________________________________________|


        public System.Timers.Timer PrisonCheck;
        public Vector3 PrisonSpawnPosition = new Vector3(1773.77200, 2551.73400, 45.56499);

        public PrisonHandler()
        {
            API.onResourceStart += OnResourceStart;
        }
        public void SpawnPlayerInJail(Client player)
        {
            API.setEntityPosition(player.handle, PrisonSpawnPosition);
            player.invincible = true;
            player.wantedLevel = 0;
            return;
        }

        public void StartJail(Client player)
        {
            var wantedsys = new Wanted_System();
            int playerwantedlevel = wantedsys.GetPlayerWanteds(player);
            int playerjailtime = playerwantedlevel * JailTime;
            API.setEntityData(player, "jailtime", playerjailtime);
            wantedsys.SetPlayerWanteds(player, 0);
            player.invincible = true;
            player.wantedLevel = 0;
            API.setEntityData(player, "arrested", true);

            API.sendChatMessageToPlayer(player, "~b~Youre arrested for " + playerjailtime + " seconds!");
            return;
        }

        public void OnResourceStart()
        {
            PrisonCheck = new System.Timers.Timer(5000);
            PrisonCheck.Elapsed += new ElapsedEventHandler(PrisonCheckStart);
            PrisonCheck.Start();
        }

        public void PrisonCheckStart(object sender, ElapsedEventArgs e)
        {
            List<Client> AllPlayers = API.getAllPlayers();
            foreach (Client player in AllPlayers)
            {
                if (API.getEntityData(player, "jailtime") >= 5)
                {
                    int oldjailtime = API.getEntityData(player, "jailtime");
                    int newjailtime = oldjailtime - 5;
                    API.setEntityData(player, "jailtime", newjailtime);
                    continue;
                }
                else if (API.getEntityData(player, "jailtime") <= 5 & API.getEntityData(player, "arrested") == true)
                {
                    ReleaseJail(player);
                    continue;
                }
            }
        }
        public void ReleaseJail(Client player)
        {
            API.setEntityData(player, "arrested", false);
            API.setEntityData(player, "jailtime", 0);
            player.invincible = false;
            var instance = new Main();
            instance.playerspawner(player);
            return;
        }

    }
}
