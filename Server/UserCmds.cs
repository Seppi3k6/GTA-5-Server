﻿using GTANetworkServer;
using GTANetworkShared;
using System;
using System.Collections.Generic;
using System.Data;

namespace Server
{
    public class UserCmds : Script
    {
        //Settings
        int wantedclearcost = 300;

        public string queryread;
        public DataTable resultread;
        public string querycount;
        public DataTable resultcount;
        public string count;
        public int intcount;

        [Command("newrobmission", Alias = "USAGE: /newrobmission [profit] [Wanteds]")]
        public void newrobmissiontodb(Client player, int profit, int wanteds)
        {
            if (API.getEntityData(player, "Adminlevel") >= 5)
            {
                var PlayerPos = player.position;

                string PosX = PlayerPos.X.ToString("0.00000");
                string PosY = PlayerPos.Y.ToString("0.00000");
                string PosZ = PlayerPos.Z.ToString("0.00000");

                PosX = PosX.Replace(",", ".");
                PosY = PosY.Replace(",", ".");
                PosZ = PosZ.Replace(",", ".");

                var robmissionprofit = profit;
                var robmissionwanteds = wanteds;

                string query = string.Format("INSERT INTO `robmissions` (`positiona`, `positionb`, `positionc`, `profit`, `wanteds`) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", PosX, PosY, PosZ, robmissionprofit, robmissionwanteds);
                API.exported.database.executeQuery(query);
                API.sendChatMessageToPlayer(player, "Neuer Robpoint wurde erstellt: Gewinn:" + robmissionprofit + ", Wanteds: " + robmissionwanteds + ".");

                Vector3 robmissionposi = PlayerPos;
                var prop = API.createObject(API.getHashKey("prop_bomb_01"), robmissionposi - new Vector3(0, 0, 1f), new Vector3());
                API.setEntityTransparency(prop, 0);
                API.setEntityCollisionless(prop, true);
                var MissionTextLabel = API.createTextLabel("Profit: " + robmissionprofit + " Wanteds: " + robmissionwanteds + " /rob", robmissionposi, 20, 1);
                API.attachEntityToEntity(MissionTextLabel, prop.handle, null, new Vector3(0, 0, 1f), new Vector3());

                var newBlip = API.createBlip(robmissionposi);
                newBlip.scale = 0.9f;
                API.setBlipSprite(newBlip, 207);
                API.setBlipShortRange(newBlip, true);
                MissionsReload();
                return;
            }
            else
            {
                API.sendChatMessageToPlayer(player, "~r~Youre not a FullAdmin!");
                return;
            }
        }

        public UserCmds()
        {
            API.onResourceStart += OnResourceStart;
        }

        public void OnResourceStart()
        {
            MissionsReload();
        }

        public void MissionsReload()
        {
            //Missions auslesen & zählen
            queryread = "SELECT * FROM `robmissions`";
            resultread = API.exported.database.executeQueryWithResult(queryread);
            querycount = "SELECT COUNT(*) FROM robmissions";
            resultcount = API.exported.database.executeQueryWithResult(querycount);
            count = resultcount.Rows[0][0].ToString();
            intcount = Int32.Parse(count);
            return;
        }

        [Command("getcar")]
        public void getcar(Client player)
        {
            if (API.getEntityData(player, "fraction") == 1)
            {
                Vector3 CopCarNPC = new Vector3(441.2433, -978.4735, 30.6896);
                List<Client> PlayersInRange = API.getPlayersInRadiusOfPosition(6, CopCarNPC);
                var exists = PlayersInRange.Contains(player);
                if (exists == true)
                {
                    API.triggerClientEvent(player, "OPEN_SELECTCOPCAR_MENU");
                    return;
                }
                else
                {
                    API.sendChatMessageToPlayer(player, "Youre not in the Police Station at the Desk!");
                    return;
                }

            }
            else if (API.getEntityData(player, "fraction") == 2)
            {
                Vector3 CriminalCarNPC = new Vector3(-651.71510, -1213.43100, 11.03362);
                List<Client> PlayersInRange = API.getPlayersInRadiusOfPosition(15, CriminalCarNPC);
                var exists = PlayersInRange.Contains(player);
                if (exists == true)
                {
                    API.triggerClientEvent(player, "OPEN_SELECTCRIMINALCAR_MENU");
                    return;
                }
                else
                {
                    API.sendChatMessageToPlayer(player, "Youre not at the Car Dealer");
                    return;
                }
            }
            else
            {
                API.sendChatMessageToPlayer(player, "ERROR: No Permission for this Command");
                return;
            }
        }
        [Command("rob")]
        public void startrob(Client player)
        {
            if (API.getEntityData(player, "fraction") != 2)
            {
                API.sendChatMessageToPlayer(player, "~r~Youre not a Criminal!");
                return;
            }
            if (API.getEntityData(player, "isinmissionrob") == true)
            {
                API.sendChatMessageToPlayer(player, "~y~Notice: ~r~Youre alrdy in a rob!");
                return;
            }

            for (int i = 0; i < intcount; i++)
            {
                string robmissionposia = resultread.Rows[i][1].ToString();
                float f1 = float.Parse(robmissionposia);
                string robmissionposib = resultread.Rows[i][2].ToString();
                float f2 = float.Parse(robmissionposib);
                string robmissionposic = resultread.Rows[i][3].ToString();
                float f3 = float.Parse(robmissionposic);
                Vector3 robmissionposi = new Vector3(f1, f2, f3);

                List<Client> PlayersInRange = API.getPlayersInRadiusOfPosition(15, robmissionposi);
                var exists = PlayersInRange.Contains(player);
                if (exists == true)
                {
                    Vector3 robmissionposition = robmissionposi;

                    string robmissionpr = resultread.Rows[i][4].ToString();
                    int robmissionprofit = int.Parse(robmissionpr);

                    string robmissionwan = resultread.Rows[i][5].ToString();
                    int robmissionwanteds = int.Parse(robmissionwan);

                    var instance = new RobMissions();
                    instance.StartRobMission(player, robmissionposition, robmissionprofit, robmissionwanteds);
                    return;
                }
            }
                API.sendChatMessageToPlayer(player, "~r~Youre not at a rob mission!");
        }
        [Command("hackwanted")]
        public void hackwanted(Client player, int wantedstohack)
        {

            if (API.getEntityData(player, "fraction") == 1)
            {
                API.sendChatMessageToPlayer(player, "You cant use this as Cop!");
                return;
            }
            if (API.getEntityData(player, "wantedlevel") == 0)
            {
                API.sendChatMessageToPlayer(player, "~y~Notice: ~r~You got no Wanteds!");
                return;
            }
            if (API.getEntityData(player, "fraction") == 2)
            {
                bool isinposition = false;
                List<Vector3> npcposwantedhackerlist = new List<Vector3>();
                Vector3 npcposwantedhacker1 = new Vector3(303.93310, -2078.92600, 17.65027);
                Vector3 npcposwantedhacker2 = new Vector3(-519.58970, 594.25390, 120.83660);
                Vector3 npcposwantedhacker3 = new Vector3(2781.10200, -706.54750, 5.05593);
                npcposwantedhackerlist.Add(npcposwantedhacker1);
                npcposwantedhackerlist.Add(npcposwantedhacker2);
                npcposwantedhackerlist.Add(npcposwantedhacker3);
                foreach (Vector3 Vector3 in npcposwantedhackerlist)
                {
                    List<Client> PlayersInRange = API.getPlayersInRadiusOfPosition(8, Vector3);
                    isinposition = PlayersInRange.Contains(player);
                    if (isinposition == true)
                    {
                        break;
                    }
                }
                if (isinposition == true)
                {
                    int oldmoney = API.getEntityData(player, "money");
                    int oldwantedlevel = API.getEntityData(player, "wantedlevel");
                    if (oldwantedlevel < wantedstohack)
                    {
                        wantedstohack = oldwantedlevel;
                    }
                    int costs = wantedstohack * wantedclearcost;
                    if (oldmoney < costs)
                    {
                        API.sendChatMessageToPlayer(player, "~y~Notice:~r~You dont have enough money! Costs: " + wantedclearcost + " / Wanted!");
                        return;
                    }
                    else
                    {
                        int newmoney = oldmoney - costs;
                        API.setEntityData(player, "money", newmoney);
                        var instance = new Wanted_System();
                        instance.ClearPlayerWanteds(player, wantedstohack);
                        API.sendChatMessageToPlayer(player, "~b~You have successfully hack " + wantedstohack + "Wanteds!");
                        API.sendNotificationToPlayer(player, "~r~- " + costs + "$");
                        API.triggerClientEvent(player, "update_money_display", API.getEntityData(player, "money"));
                        return;
                    }
                }
                else
                {
                    API.sendChatMessageToPlayer(player, "~r~You're not at the position of a Hacker!");
                    return;
                }
            }
        }

        [Command("deleterobmission")]
        public void deleterobmission(Client player)
        {
            if (API.getEntityData(player, "Adminlevel") >= 5)
            {
                for (int b = 0; b < intcount; b++)
                {
                    string robmissionposia = resultread.Rows[b][1].ToString();
                    float f1 = float.Parse(robmissionposia);
                    string robmissionposib = resultread.Rows[b][2].ToString();
                    float f2 = float.Parse(robmissionposib);
                    string robmissionposic = resultread.Rows[b][3].ToString();
                    float f3 = float.Parse(robmissionposic);
                    Vector3 robmissionposi = new Vector3(f1, f2, f3);

                    List<Client> PlayersInRange = API.getPlayersInRadiusOfPosition(10, robmissionposi);
                    var exists = PlayersInRange.Contains(player);
                    if (exists == true)
                    {
                        string PosX = robmissionposi.X.ToString("0.00000");
                        string PosY = robmissionposi.Y.ToString("0.00000");
                        string PosZ = robmissionposi.Z.ToString("0.00000");

                        string PosX2 = PosX.Replace(",", ".");
                        string PosY2 = PosY.Replace(",", ".");
                        string PosZ2 = PosZ.Replace(",", ".");

                        string query = string.Format("DELETE FROM `robmissions` WHERE `positiona` = '" + PosX2 + "' AND `positionb` = '" + PosY2 + "' AND `positionc` = '" + PosZ2 + "'");
                        API.exported.database.executeQuery(query);
                        API.sendChatMessageToPlayer(player, "~g~Robmission wurde gelöscht!");

                        double x = double.Parse(PosX);
                        double y = double.Parse(PosY);
                        double z = double.Parse(PosZ);
                        double textz1 = z + 2.0;
                        Vector3 realtextposi1 = new Vector3(x, y, textz1);

                        List<NetHandle> alllabels = API.getAllLabels();
                        foreach (NetHandle textlabel in alllabels)
                        {
                            Vector3 labelpos = API.getEntityPosition(textlabel);
                            if (realtextposi1 == labelpos)
                            {
                                API.deleteEntity(textlabel);
                            }
                            else
                            {
                                continue;
                            }
                        }
                        List<NetHandle> alllblips = API.getAllBlips();
                        foreach (NetHandle blip in alllabels)
                        {
                            var blippos = API.getEntityPosition(blip);
                            if (blippos == robmissionposi)
                            {
                                API.deleteEntity(blip);
                            }
                            else
                            {
                                continue;
                            }
                        }
                        MissionsReload();
                    }
                }
            }
            else
            {
                API.sendChatMessageToPlayer(player, "~r~Youre not a FullAdmin!");
                return;
            }
        }
    }
}
