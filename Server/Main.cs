﻿using System;
using System.Data;
using GTANetworkServer;
using GTANetworkShared;
using System.Collections.Generic;
using System.Timers;
using BCr = BCrypt.Net;

namespace Server
{
    public class Main : Script
    {

        private DateTime m_tmLastSync = DateTime.Now;

        public System.Timers.Timer RangeRadar;
        

        public Main()
        {
            API.onUpdate += OnUpdateHandler;
            API.onResourceStart += OnResourceStart;
            API.onResourceStop += OnResourceStop;
            API.onChatMessage += OnSentMessage;
            API.onPlayerConnected += OnPlayerConnected;
            API.onPlayerDisconnected += OnPlayerDisconnected;
            API.onPlayerEnterVehicle += OnPlayerEnterVehicle;
            API.onPlayerExitVehicle += OnPlayerExitVehicleHandler;
            API.onPlayerFinishedDownload += OnPlayerFinishedDownloadHandler;
            API.onChatCommand += OnChatCommand;
            API.onClientEventTrigger += onClientEventTrigger;
            API.onUpdate += API_onUpdate;
            API.onPlayerDeath += OnPlayerDeathHandler;
            API.onPlayerRespawn += OnPlayerRespawnHandler;
        }
        public void onClientEventTrigger(Client sender, string name, object[] args)
        {
            if(name == "SELECT_ADM_GOTO")
            {
                string Selected = (string)args[0];
                switch(Selected)
                {
                    case "Criminal-Spawn":
                        API.setEntityPosition(sender, new Vector3(-643, -1228, 11.54758));
                        break;

                    case "Police-Spawn":
                        API.setEntityPosition(sender, new Vector3(458, -990, 30.6896));
                        break;

                    default:
                        API.kickPlayer(sender, "kick1");
                        break;
                }
            }
            if (name == "SELECT_FRAKTION")
            {
                string Fraktion = (string)args[0];
                if(Fraktion == "Cop")
                {
                    API.setEntityData(sender, "fraction", 1);
                    playerspawner(sender);
                    return;
                }
                else if(Fraktion == "Criminal")
                {
                    API.setEntityData(sender, "fraction", 2);
                    playerspawner(sender);
                    return;
                }
                else
                {
                    API.kickPlayer(sender, "kick2");

                    return;
                }
            }
            if (name == "REGISTER")
            {
                string password = (string)args[0];
                var hash = BCr.BCrypt.HashPassword(password, BCr.BCrypt.GenerateSalt(12));

                API.exported.database.executeQuery("INSERT INTO users (username, password) VALUES ('" + sender.name + "', '" + hash +"')");
                API.sendChatMessageToPlayer(sender, "Registration complete!");
                loginScreen(sender);
                return;
            }

            if (name == "LOGIN")
            {
                string password = (string)args[0];

                string query = "SELECT password FROM users WHERE username='" + sender.name + "'";
                DataTable result = API.exported.database.executeQueryWithResult(query);
                string databasepasswordhash = result.Rows[0][0].ToString();
                bool isPasswordCorrect = BCr.BCrypt.Verify(password, databasepasswordhash);

                int WrongPasswordEntrysCount = API.getEntityData(sender, "WrongPasswordEntrys");
                //int WrongPasswordEntrysCount = Int32.Parse(WrongPasswordEntrys);

                if (isPasswordCorrect)
                {
                    //setplayerdata
                    var instance = new DatabaseSync();
                    instance.GetPlayerDatabaseStats(sender);
                    API.setEntityData(sender, "Loggedin", 1);

                    if (API.getEntityData(sender, "Adminlevel") >= 1)
                    {
                        API.sendChatMessageToPlayer(sender, "~b~" + sender.name + "~g~ logged in! Admin Rank: " + API.getEntityData(sender, "Adminlevel"));
                    }
                    else
                    {
                        API.sendChatMessageToPlayer(sender, "~b~" + sender.name + "~g~ logged in!");
                    }

                    login(sender);
                    return;
                }
                else
                {
                    if (WrongPasswordEntrysCount >= 3)
                    {
                        API.kickPlayer(sender, "To many password failures");

                        return;
                    }
                    WrongPasswordEntrysCount ++;
                    API.setEntityData(sender, "WrongPasswordEntrys", WrongPasswordEntrysCount);
                    API.sendChatMessageToPlayer(sender, "~r~Wrong Password!");
                    loginScreen(sender);
                    return;
                }
            }
            if (name == "SELECT_COPCAR")
            {
                string Copcar = (string)args[0];
                Random rnd = new Random();
                int dice = rnd.Next(1, 5);
                dice--;

                 Vector3 position1 = new Vector3(463.29260, -1019.26100, 27.69930);
                 Vector3 rotation1 = new Vector3(-0.03211894, 0.2661464, 90.77341);
                 Vector3 position2 = new Vector3(462.53230, -1014.68800, 27.68283);
                 Vector3 rotation2 = new Vector3(0.2273132, 0.2461804, 88.15952);
                 Vector3 position3 = new Vector3(452.43390, -996.69390, 25.37406);
                 Vector3 rotation3 = new Vector3(-0.8381287, 0.02200139, -178.6765);
                 Vector3 position4 = new Vector3(447.34190, -996.62010, 25.37483);
                 Vector3 rotation4 = new Vector3(-0.8274256, -0.004595278, 179.2328);
                 List<Vector3> Positionlist = new List<Vector3>();
                 Positionlist.Add(position1);
                 Positionlist.Add(position2);
                 Positionlist.Add(position3);
                 Positionlist.Add(position4);
                 List<Vector3> Rotationlist = new List<Vector3>();
                 Rotationlist.Add(rotation1);
                 Rotationlist.Add(rotation2);
                 Rotationlist.Add(rotation3);
                 Rotationlist.Add(rotation4);

                spawncar(sender, Positionlist[dice], Rotationlist[dice], Copcar, 0, 0);
                return;
            }
            if (name == "SELECT_CRIMINALCAR")
            {
                string Criminalcar = (string)args[0];

                if (Criminalcar == "Sultan" & API.getEntityData(sender, "Level") <= 2)
                {
                    API.sendChatMessageToPlayer(sender, "~y~Notification: ~r~ Your level isnt high enogh for this car!");
                    return;
                }
                if (Criminalcar == "Elegy" & API.getEntityData(sender, "Level") <= 6)
                {
                    API.sendChatMessageToPlayer(sender, "~y~Notification: ~r~ Your level isnt high enogh for this car!");
                    return;
                }

                Random rnd = new Random();
                int dice = rnd.Next(1, 5);
                dice--;

                Vector3 position1 = new Vector3(-612.66140, -1210.88100, 13.74350);
                Vector3 rotation1 = new Vector3(5.678074, -0.05401101, -50.35154);
                Vector3 position2 = new Vector3(-622.79640, -1187.93100, 14.54990);
                Vector3 rotation2 = new Vector3(-0.2408223, -2.296233, 44.57251);
                Vector3 position3 = new Vector3(-605.46830, -1179.04600, 15.70319);
                Vector3 rotation3 = new Vector3(-0.4606619, 4.453916, -177.6765);
                Vector3 position4 = new Vector3(-576.14940, -1179.05400, 17.82398);
                Vector3 rotation4 = new Vector3(0.06826106, 4.218488, -176.8225);
                List<Vector3> Positionlist = new List<Vector3>();
                Positionlist.Add(position1);
                Positionlist.Add(position2);
                Positionlist.Add(position3);
                Positionlist.Add(position4);
                List<Vector3> Rotationlist = new List<Vector3>();
                Rotationlist.Add(rotation1);
                Rotationlist.Add(rotation2);
                Rotationlist.Add(rotation3);
                Rotationlist.Add(rotation4);

                spawncar(sender, Positionlist[dice], Rotationlist[dice], Criminalcar, 0, 0);
                return;
            }
        }

        public void OnUpdateHandler()
        {
            //
        }
        public void OnPlayerFinishedDownloadHandler(Client player)
        {
            //Checkt ob User existiert
            string query = "SELECT COUNT(*) FROM users WHERE username='" + player.name + "' LIMIT 1";
            DataTable result = API.exported.database.executeQueryWithResult(query);
            string playerdatabasename = result.Rows[0][0].ToString();
            int x = Int32.Parse(playerdatabasename);

            if (x > 0)
            {
                loginScreen(player);
            }
            else
            {
                //API.kickPlayer(player, "Server is on Dev, come back later! :-)");
                registerScreen(player);
            }
        }
        public void OnPlayerConnected(Client player)
        {
            Vector3 ConnectSpawn = new Vector3(-1639.90400, -1482.74100, 81.57152);
            Vector3 ConnectSpawnrot = new Vector3(-3.769287, -7.390397, -45.02374);
            player.freezePosition = true;
            API.setEntityPosition(player.handle, ConnectSpawn);
            API.setEntityRotation(player.handle, ConnectSpawnrot);
            API.sendChatMessageToAll(player.name +" hat den Server betreten.");
            API.consoleOutput("[Connect]" + player.name + " hat den Server betreten.");
            player.invincible = true;

            API.setEntityData(player, "WrongPasswordEntrys", 0);
            API.setEntityData(player, "fraction", 0);
            API.setEntityData(player, "Bigears", 0);

        }
        public void OnPlayerDisconnected(Client player, string reason)
        {
            SavePlayer(player);
            API.setEntityData(player, "Loggedin", 0);
            API.sendChatMessageToAll(player.name + " hat den Server verlassen.");
            API.consoleOutput("[Disconnected]" + player.name + " hat den Server verlassen[" + reason + "]");
            RemoveBlipForPlayer(player);
        }
        public void OnResourceStart()
        {
            API.consoleOutput("Der Server wurde gestartet");

            RangeRadar = new System.Timers.Timer(1000);
            RangeRadar.Elapsed += new ElapsedEventHandler(SearchingForCriminalshandler);
            RangeRadar.Start();

            

            //copnpc an den Schreibtisch stellen
            Vector3 npcposcop = new Vector3(441.2433, -978.4735, 30.6896);
            string npccopskin = "CopCutscene";
            var coppedskin = API.createPed(API.pedNameToModel(npccopskin), npcposcop, 180, 0);
            var coptextlabel = API.createTextLabel("/getcar to get your police car", npcposcop, 20, 1);
            API.attachEntityToEntity(coptextlabel, coppedskin.handle, null, new Vector3(0, 0, 1f), new Vector3());

            //criminalnpc an die Stelle stellen
            Vector3 npcposcriminal = new Vector3(-651.71510, -1213.43100, 11.03362);
            string npcskincriminal = "ArmyMech01SMY";
            var criminalpedskin = API.createPed(API.pedNameToModel(npcskincriminal), npcposcriminal, -62, 0);
            var criminaltextlabel = API.createTextLabel("/getcar to get your car", npcposcriminal, 20, 1);
            API.attachEntityToEntity(criminaltextlabel, criminalpedskin.handle, null, new Vector3(0, 0, 1f), new Vector3());

            //ClearWanted Typen
            //#1
            Vector3 npcposwantedhacker1 = new Vector3(303.93310, -2078.92600, 17.65027);
            string npcskinwantedhacker1 = "ArmyMech01SMY";
            var wantedhacker1pedskin = API.createPed(API.pedNameToModel(npcskinwantedhacker1), npcposwantedhacker1, -83, 0);
            var wantedhacker1textlabel = API.createTextLabel("/hackwanted to hack your Wanted´s", npcposwantedhacker1, 20, 1);
            API.attachEntityToEntity(wantedhacker1textlabel, wantedhacker1pedskin.handle, null, new Vector3(0, 0, 1f), new Vector3());
            var newBlip1 = API.createBlip(npcposwantedhacker1);
            newBlip1.scale = 0.9f;
            API.setBlipSprite(newBlip1, 521);
            API.setBlipShortRange(newBlip1, true);
            //#2
            Vector3 npcposwantedhacker2 = new Vector3(-519.58970, 594.25390, 120.83660);
            string npcskinwantedhacker2 = "ArmyMech01SMY";
            var wantedhacker2pedskin = API.createPed(API.pedNameToModel(npcskinwantedhacker2), npcposwantedhacker2, -81, 0);
            var wantedhacker2textlabel = API.createTextLabel("/hackwanted to hack your Wanted´s", npcposwantedhacker2, 20, 1);
            API.attachEntityToEntity(wantedhacker2textlabel, wantedhacker2pedskin.handle, null, new Vector3(0, 0, 1f), new Vector3());
            var newBlip2 = API.createBlip(npcposwantedhacker2);
            newBlip2.scale = 0.9f;
            API.setBlipSprite(newBlip2, 521);
            API.setBlipShortRange(newBlip2, true);
            //#3
            Vector3 npcposwantedhacker3 = new Vector3(2781.10200, -706.54750, 5.05593);
            string npcskinwantedhacker3 = "ArmyMech01SMY";
            var wantedhacker3pedskin = API.createPed(API.pedNameToModel(npcskinwantedhacker3), npcposwantedhacker3, 132, 0);
            var wantedhacker3textlabel = API.createTextLabel("/hackwanted to hack your Wanted´s", npcposwantedhacker3, 20, 1);
            API.attachEntityToEntity(wantedhacker3textlabel, wantedhacker3pedskin.handle, null, new Vector3(0, 0, 1f), new Vector3());
            var newBlip3 = API.createBlip(npcposwantedhacker3);
            newBlip3.scale = 0.9f;
            API.setBlipSprite(newBlip3, 521);
            API.setBlipShortRange(newBlip3, true);
        }

        public void SearchingForCriminalshandler(object sender, ElapsedEventArgs e)
        {
            /*List<Client> AllPlayers = API.getAllPlayers();
            foreach (Client player in AllPlayers)
            {
                if (API.getEntityData(player, "fraction") == 2)
                {
                    return;
                }
                if (API.getEntityData(player, "fraction") == 1 & API.getEntityData(player, "isinpursuit") == false & API.isPlayerInAnyVehicle(player) == true)
                {
                    var instance = new RangeRadarSearchForCriminals();
                    instance.StartSearchingForCriminals(player);
                }
            }
            return;*/
        }

        public void OnResourceStop()
        {
            List<Client> AllPlayers = API.getAllPlayers();
            foreach (Client player in AllPlayers)
            {
                SavePlayer(player);
                API.setEntityData(player, "Loggedin", 0);
                API.kickPlayer(player, "Server is shutting down!");
            }
            API.consoleOutput("Der Server wurde gestopt");
        }
        public void OnSentMessage(Client player, string message, CancelEventArgs e)
        {
            Functions func = new Functions();
            func.ProxDetector(30, player, player.name.Replace("_", " ") + " sagt: " + message, "~#FFFFFF~", "~#C8C8C8~", "~#AAAAAA~", "~#8C8C8C~", "~#6E6E6E~");
            API.consoleOutput(player.name.Replace("_", " ") + " sagt: " + message);

            var instance = new ChatMessageFunction();
            instance.SendBigearsMessage(player.name + " sagt: " + message);

            e.Cancel = true;
            return;
        }
        public void OnChatCommand(Client player, string command, CancelEventArgs e)
        {
            API.sendChatMessageToPlayer(player, "Command: " + command);
        }

        public void loginScreen(Client player)
        {
            API.triggerClientEvent(player, "OPEN_LOGIN_SCREEN");
        }

        public void registerScreen(Client player)
        {
            API.triggerClientEvent(player, "OPEN_REGISTER_SCREEN");
        }

        public void login(Client player)
        {
            API.triggerClientEvent(player, "OPEN_LOGIN_MENU");
        }

        public void OnPlayerExitVehicleHandler(Client player, NetHandle vehicle)
        {
            if (API.getEntityData(player, "fraction") == 1 & API.getEntityData(player, "RangeRadarStatus") == true)
            {
                var instance = new RangeRadarSearchForCriminals();
                instance.RangeRadarStop(player, vehicle);
                API.setEntityData(player, "RangeRadarStatus", false);
            }
        }

        public void OnPlayerEnterVehicle(Client player, NetHandle vehicle)
        {
            if (API.getEntityData(player, "fraction") == 1)
            {
                API.setEntityData(player, "RangeRadarStatus", true);

                var instance = new RangeRadarSearchForCriminals();
                instance.RangeRadarStart(player, vehicle);
            }
        }

        public void SavePlayer(Client player)
        {
            if (API.getEntityData(player, "Loggedin") == 1)
            {
                var instance = new DatabaseSync(); //Save stats
                instance.SetPlayerDatabaseStats(player); //Save stats
                return;
            }
            else
            {
                return;
            }
        }
        
        public void spawncar(Client player, Vector3 position, Vector3 rotation, string car, int col1, int col2)
        {
            Vector3 vehPos = position;
            VehicleHash myVehicle = API.vehicleNameToModel(car);
            var vehID = API.createVehicle(myVehicle, vehPos, rotation, col1, col2);
            API.setPlayerIntoVehicle(player, vehID, -1);
            return;
        }

        public void AddBlipForPlayer(Client player, int blipcolor)
        {
            RemoveBlipForPlayer(player);

            var newBlip = API.createBlip(player.position);
            newBlip.scale = 0.9f;
            API.setBlipColor(newBlip, blipcolor);

            player.setData("playerblip", newBlip);
            player.setData("playerblip_rotation", player.rotation.Z);
            player.setSyncedData("playerblip", newBlip.handle);
        }

        public void RemoveBlipForPlayer(Client player)
        {
            var blip = GetBlipForPlayer(player);
            if (blip == null)
            {
                return;
            }

            blip.delete();
            player.resetData("playerblip");
            player.resetSyncedData("playerblip");
        }

        public Blip GetBlipForPlayer(Client player)
        {
            return player.getData("playerblip");
        }

        public void API_onUpdate()
        {
            //Blip updater
            if ((DateTime.Now - m_tmLastSync).TotalMilliseconds >= 100)
            {
                var players = API.getAllPlayers();
                foreach (var player in players)
                {
                    var blip = GetBlipForPlayer(player);
                    if (blip == null)
                    {
                        continue;
                    }

                    if (blip.position.DistanceTo(player.position) >= 2.0f)
                    {
                        blip.position = player.position;
                    }
                    if (Math.Abs(player.rotation.Z - (float)player.getData("playerblip_rotation")) > 10.0f)
                    {
                        if (blip.sprite > 1)
                        {
                            API.sendNativeToAllPlayers(Hash.SET_BLIP_ROTATION, blip.handle, (int)player.rotation.Z);
                        }
                        player.setData("playerblip_rotation", player.rotation.Z);
                    }
                }
            }
            //SetBlipForCriminalsIfWanted
            //setting
            int highwanted = 4;
            int lowwanted = 3;
            int highwantedcolor = 27;
            int lowwantedcolor = 1;

            if ((DateTime.Now - m_tmLastSync).TotalMilliseconds >= 500)
            {
                var players = API.getAllPlayers();
                foreach (var player in players)
                {
                    /**if (API.getEntityData(player, "loggedin") != 1)
                    {
                        continue;
                    }*/
                    if (API.getEntityData(player, "wantedlevel") >= 1 & API.getEntityData(player, "isblipactive") == false)
                    {
                        if (API.getEntityData(player, "wantedlevel") >= highwanted)
                        {
                            AddBlipForPlayer(player, highwantedcolor);
                        }
                        else
                        {
                            AddBlipForPlayer(player, lowwantedcolor);
                        }
                        API.setEntityData(player, "isblipactive", true);
                    }
                    else if (API.getEntityData(player, "wantedlevel") >= 1 & API.getEntityData(player, "isblipactive") == true)
                    {
                        if (API.getEntityData(player, "wantedlevel") <= lowwanted & API.getEntityData(player, "isblipactive") == true & API.getBlipColor(GetBlipForPlayer(player)) == highwantedcolor)
                        {
                            AddBlipForPlayer(player, lowwantedcolor);
                            API.setEntityData(player, "isblipactive", true);
                        }
                        else if (API.getEntityData(player, "wantedlevel") >= highwanted & API.getEntityData(player, "isblipactive") == true & API.getBlipColor(GetBlipForPlayer(player)) == lowwantedcolor)
                        {
                            AddBlipForPlayer(player, highwantedcolor);
                            API.setEntityData(player, "isblipactive", true);
                        }
                    }
                    else if (API.getEntityData(player, "wantedlevel") <= 0 & API.getEntityData(player, "isblipactive") == true)
                    {
                        RemoveBlipForPlayer(player);
                        API.setEntityData(player, "isblipactive", false);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            //Rangeradarchecker
            if ((DateTime.Now - m_tmLastSync).TotalMilliseconds >= 200)
            {
                List<Client> AllPlayers = API.getAllPlayers();
                foreach (Client player in AllPlayers)
                {
                    if (API.getEntityData(player, "fraction") == 1 & API.getEntityData(player, "isinpursuit") == false & API.isPlayerInAnyVehicle(player) == true & API.getEntityData(player, "StartSearchingForCriminals") != true)
                    {
                        var instance = new RangeRadarSearchForCriminals();
                        instance.StartSearchingForCriminals(player);
                    }
                }
                return;
            }
        }
        private void OnPlayerDeathHandler(Client player, NetHandle entityKiller, int weapon)
        {
            var Wantedsys = new Wanted_System();
            var UserFunc = new UserFunctions();
            Client killer = API.getPlayerFromHandle(entityKiller);
            if (killer != null)
            {
                if (UserFunc.GetPlayerFraction(player) == 2 && UserFunc.GetPlayerFraction(killer) == 1 && Wantedsys.GetPlayerWanteds(player) >= 1) //1 == Cop  | 2 == Criminal
                {
                    var PrisonFunc = new PrisonHandler();
                    PrisonFunc.StartJail(player);
                }
            }
            
            /**if (API.getEntityData(player, "wanteds") >= 1)
            {
                if (API.getEntityData(player, "isinmissionrob") == true & API.getEntityData(killer, "fraction") == 1)
                {
                    API.setEntityData(player, "isinmissionrob", false);
                    int CopCounter = 0;
                    int revenue = API.getEntityData(player, "wanteds") * 100;

                    List<Client> PlayersInCopRange = API.getPlayersInRadiusOfPosition(20, killer.position);
                    foreach (Client playera in PlayersInCopRange)
                    {
                        if (API.getEntityData(playera, "fraction") == 1)
                        {
                            CopCounter++;
                        }
                    }
                    foreach (Client playera in PlayersInCopRange)
                    {
                        if (API.getEntityData(playera, "fraction") == 1)
                        {
                            double sharedrevenue = revenue / CopCounter;
                            double sharedrevenuerounded = Math.Ceiling(sharedrevenue);
                            int money = API.getEntityData(playera, "money") + sharedrevenuerounded;
                            API.setEntityData(playera, "money", money);
                            API.sendNotificationToPlayer(playera, "~g~+ " + money + "$");
                            API.triggerClientEvent(playera, "update_money_display", API.getEntityData(playera, "money"));
                        }
                    }
                    API.sendChatMessageToPlayer(player, "~r~Mission failed, you was killed by Cop!");
                    API.sendNotificationToPlayer(killer, "~g~You killed the criminal " + player.name + "!");
                    var instance = new PrisonHandler();
                    instance.StartJail(player);
                }
                API.setEntityData(player, "arrested", true);
                API.setEntityData(player, "isinmissionrob", false);
                return;
            }**/

        }
        private void OnPlayerRespawnHandler(Client player)
        {
            playerspawner(player);
        }
        public void playerspawner(Client player)
        {
            if (API.getEntityData(player, "jailtime") >= 5)
            {
                var PrisonFunc = new PrisonHandler();
                PrisonFunc.SpawnPlayerInJail(player);
                return;
            }
            if (API.getEntityData(player, "fraction") == 1)
            {
                //copspawn & default cop skin
                Vector3 CopSpawn = new Vector3(458.4821, -990.9999, 30.6896);
                string DefaultCopSkin = "CopCutscene";

                API.setEntityPosition(player.handle, CopSpawn);
                API.setPlayerSkin(player, API.pedNameToModel(DefaultCopSkin));
                player.invincible = false;
                API.givePlayerWeapon(player, WeaponHash.Pistol, 100, true, true);
                API.givePlayerWeapon(player, WeaponHash.CarbineRifle, 300, false, true);
                API.givePlayerWeapon(player, WeaponHash.PumpShotgun, 20, false, true);
                API.setEntityData(player, "isinpursuit", false);
                API.setPlayerTeam(player, 1);
                AddBlipForPlayer(player, 2);
                API.setEntityData(player, "isblipactive", true);
                API.setPlayerWantedLevel(player, 0);
                player.freezePosition = false;
                API.sendNativeToPlayer(player, Hash.SET_PED_CAN_RAGDOLL, player.handle, false);
                return;
            }
            else if (API.getEntityData(player, "fraction") == 2)
            {
                //criminalspawn & default criminal skin
                Vector3 CriminalSpawn = new Vector3(-643.0175, -1228.023, 11.54758);
                string DefaultCriminalSkin = "SouCent04AMM";

                API.setEntityPosition(player.handle, CriminalSpawn);
                API.setPlayerSkin(player, API.pedNameToModel(DefaultCriminalSkin));
                player.invincible = false;
                API.givePlayerWeapon(player, WeaponHash.HeavyPistol, 100, true, true);
                API.givePlayerWeapon(player, WeaponHash.AssaultRifle, 300, false, true);
                API.givePlayerWeapon(player, WeaponHash.SawnoffShotgun, 20, false, true);
                API.setEntityData(player, "arrested", false);
                API.setPlayerTeam(player, 2);
                API.setEntityData(player, "isblipactive", false);
                player.freezePosition = false;
                API.sendNativeToPlayer(player, Hash.SET_PED_CAN_RAGDOLL, player.handle, false);
                return;
            }
            else
            {
                API.kickPlayer(player);
                return;
            }
        }
    }
}

