﻿using GTANetworkServer;
using GTANetworkShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Server
{
    public class RangeRadarSearchForCriminals : Script
    {
        //Radar timer & vars ...start
        private Timer Arrest;
        private Client playera;
        private Client playerb;
        private Client targetb;
        public int progress;
        //Radar timer & vars ...ende

        public void RangeRadarStart(Client player, NetHandle vehicle)
        {
            //Cops initialisieren
            List<string> Cops = new List<string>();

            var isplayercop = API.getEntityData(player, "fraction");

            if (isplayercop == 1)
            {
                var IsPlayerInList = Cops.Contains(player.name);
                
                if (IsPlayerInList != true)
                {
                    Cops.Add(player.name);
                }
                API.sendNotificationToPlayer(player, "~g~Range-Radar started, youre searching for criminals!");
            }
            else
            {
                return;
            }
        }

        public void StartSearchingForCriminals(Client player)
        {
            List<Client> PlayersInRange = API.getPlayersInRadiusOfPlayer(10, player);
            API.setEntityData(player, "StartSearchingForCriminals", true);

            foreach (Client target in PlayersInRange)
            {
                if (API.getEntityData(target, "fraction") == 2 & API.getEntityData(target, "arrested") != true & API.getEntityData(target, "wantedlevel") >= 1 & API.isPlayerInAnyVehicle(target) == true)
                {
                    API.setEntityData(player, "isinpursuit", true);
                    API.sendChatMessageToPlayer(player, "Du bist in der naehe eines Kriminellen, bleibe 5 Sekunden dran! ");
                    API.sendChatMessageToPlayer(target, "Ein Cop befindet sich in deiner Naehe, verschwinde! Du hast 5 Sekunden Zeit!");
                    Arrest = new Timer(200);
                    Arrest.Elapsed += new ElapsedEventHandler(ArrestCriminal);
                    playerb = player;
                    targetb = target;
                    Arrest.Start();
                    progress = 50;
                    API.triggerClientEvent(playerb, "update_progress_display", progress);
                    API.triggerClientEvent(targetb, "update_progress_display", progress);
                    return;
                }
                else
                {
                    continue;
                }
            }
            API.setEntityData(player, "StartSearchingForCriminals", false);
            return;
        }

        private void ArrestCriminal(object source, ElapsedEventArgs e)
        {
            List<Client> PlayersInRange = API.getPlayersInRadiusOfPlayer(10, playerb);
            var exists = PlayersInRange.Contains(targetb);
            if (progress == 100)
            {
                API.setEntityData(targetb, "arrested", true);
                Arrest.Stop();
                var vehicle = API.getPlayerVehicle(playerb);
                API.setPlayerIntoVehicle(targetb, vehicle, 1);
                API.sendChatMessageToPlayer(playerb, "Verhaftung erfolgreich!");
                API.sendChatMessageToPlayer(targetb, "Du wurdest verhaftet!");

                var money = API.getEntityData(playerb, "money") + 50;
                API.setEntityData(playerb, "money", money);
                API.triggerClientEvent(playerb, "update_money_display", API.getEntityData(playerb, "money"));
                API.sendNotificationToPlayer(playerb, "~h~ ~u~ + ~g~50$");

                API.setEntityData(playerb, "isinpursuit", false);
                progress = 0;
                playerb.setSyncedData("progress", progress);
                targetb.setSyncedData("progress", progress);
                API.triggerClientEvent(playerb, "update_progress_display", progress);
                API.triggerClientEvent(targetb, "update_progress_display", progress);
                var instance = new PrisonHandler();
                instance.StartJail(targetb);
                instance.SpawnPlayerInJail(targetb);
                return;
            }
            else if (progress == 0)
            {
                Arrest.Stop();
                API.sendChatMessageToPlayer(playerb, "Verhaftung fehlgeschlagen!");
                API.sendChatMessageToPlayer(targetb, "Du bist entkommen!");
                API.setEntityData(playerb, "isinpursuit", false);
                progress = 0;
                playerb.setSyncedData("progress", progress);
                targetb.setSyncedData("progress", progress);
                API.triggerClientEvent(playerb, "update_progress_display", progress);
                API.triggerClientEvent(targetb, "update_progress_display", progress);
                return;
            }
            else if (exists == false)
            {
                progress--;
                playerb.setSyncedData("progress", progress);
                targetb.setSyncedData("progress", progress);
                API.triggerClientEvent(playerb, "update_progress_display", progress);
                API.triggerClientEvent(targetb, "update_progress_display", progress);
            }
            else if (API.getEntityData(targetb, "arrested") == true)
            {
                Arrest.Stop();
                progress = 0;
                API.sendChatMessageToPlayer(playerb, "Spieler wurde bereits gefasst!");
                API.setEntityData(playerb, "isinpursuit", false);
                playerb.setSyncedData("progress", progress);
                API.triggerClientEvent(playerb, "update_progress_display", progress);
                API.triggerClientEvent(targetb, "update_progress_display", progress);
                return;
            }
            else
            {
                progress++;
                playerb.setSyncedData("progress", progress);
                targetb.setSyncedData("progress", progress);
                API.triggerClientEvent(playerb, "update_progress_display", progress);
                API.triggerClientEvent(targetb, "update_progress_display", progress);
            }
        }

        public void RangeRadarStop (Client player, NetHandle vehicle)
        {
            API.sendChatMessageToPlayer(player, "Radar ausgeschaltet!");
            return;
        }

    }
}
