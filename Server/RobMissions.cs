﻿using System;
using System.Data;
using GTANetworkServer;
using GTANetworkShared;
using System.Collections.Generic;
using System.Timers;
using BCr = BCrypt.Net;

namespace Server
{
    public class RobMissions : Script
    {

        //Radar timer & vars ...start
        private Timer RobTimer;
        private Client playera;
        private int progress;
        private Vector3 positiona;
        private int LeftAreaCounter;
        private int totalprofit;
        //Radar timer & vars ...ende

        public string queryread;
        public DataTable resultread;
        public string querycount;
        public DataTable resultcount;
        public string count;
        public int intcount;

        public RobMissions()
        {
            API.onResourceStart += OnResourceStart;
        }

        public void OnResourceStart()
        {
            //Missions auslesen & zählen
            queryread = "SELECT * FROM `robmissions`";
            resultread = API.exported.database.executeQueryWithResult(queryread);
            querycount = "SELECT COUNT(*) FROM robmissions";
            resultcount = API.exported.database.executeQueryWithResult(querycount);
            count = resultcount.Rows[0][0].ToString();
            intcount = Int32.Parse(count);

            for (int i = 0; i < intcount; i++)
            {
                string robmissionposia = resultread.Rows[i][1].ToString();
                double f1 = double.Parse(robmissionposia);
                string robmissionposib = resultread.Rows[i][2].ToString();
                double f2 = double.Parse(robmissionposib);
                string robmissionposic = resultread.Rows[i][3].ToString();
                double f3 = double.Parse(robmissionposic);

                Vector3 robmissionposi = new Vector3(f1, f2, f3);

                double textz1 = f3 + 2;

                Vector3 textposi1 = new Vector3(f1, f2, textz1);

                var MissionTextLabel = API.createTextLabel("RobMissionName\n ~y~Profit: ~g~ " + resultread.Rows[i][4] + "$\n ~y~Wanteds: ~g~" + resultread.Rows[i][5] + "\n start with /rob", textposi1, 20, 1);

                var newBlip = API.createBlip(robmissionposi);
                newBlip.scale = 0.9f;
                API.setBlipSprite(newBlip, 207);
                API.setBlipShortRange(newBlip, true);
            }
        }

        public void StartRobMission(Client player, Vector3 position, int profit, int wanteds)
        {
            List<Client> PlayersInRange = API.getPlayersInRadiusOfPosition(15, position);
            var exists = PlayersInRange.Contains(player);

            if (exists == false)
            {
                API.sendChatMessageToPlayer(player, "~r~Youre not at a rob mission!");
                return;
            }

            int playerwanteds = API.getEntityData(player, "wantedlevel");
            int missionwanteds = wanteds;
            int wantedset = playerwanteds + missionwanteds;
            API.setPlayerWantedLevel(player, wantedset);
            API.setEntityData(player, "wantedlevel", wantedset);

            API.setEntityData(player, "isinmissionrob", true);
            totalprofit = profit;

            RobTimer = new Timer(1000);
            RobTimer.Elapsed += new ElapsedEventHandler(RobMissionStatus);
            playera = player;
            positiona = position;
            RobTimer.Start();
            progress = 0;
            LeftAreaCounter = 0;

            API.sendChatMessageToPlayer(player, "~y~Notification: ~g~Rob started, stay 60 Seconds in this Area to get the complete profit!");
        }
        private void RobMissionStatus(object source, ElapsedEventArgs e)
        {
            List<Client> PlayersInRange = API.getPlayersInRadiusOfPosition(15, positiona);
            var exists = PlayersInRange.Contains(playera);
            if (API.getEntityData(playera, "isinmissionrob") == false | API.getEntityData(playera, "arrested") == true)
            {
                RobTimer.Stop();
                progress = 0;
                API.sendChatMessageToPlayer(playera, "~y~Notification: ~r~Rob canceled, you died!");
                return;
            }
            if (progress == 60)
            {
                var instance = new UserFunctions();
                instance.GivePlayerPoints(playera, 50);

                API.sendChatMessageToPlayer(playera, "~y~Notification: ~g~Rob succes! Escape from here!");
                int totalprofitper6 = totalprofit / 10;
                int money = API.getEntityData(playera, "money") + totalprofitper6;
                API.setEntityData(playera, "money", money);
                API.sendNotificationToPlayer(playera, "~g~+ " + totalprofitper6 + "$");
                API.triggerClientEvent(playera, "update_money_display", API.getEntityData(playera, "money"));
                API.setEntityData(playera, "isinmissionrob", false);
                RobTimer.Stop();
                progress = 0;
            }
            if (exists == false)
            {
                if (LeftAreaCounter >= 3)
                {
                    RobTimer.Stop();
                    progress = 0;
                    API.setEntityData(playera, "isinmissionrob", false);
                    API.sendChatMessageToPlayer(playera, "~y~Notification: ~r~You left the Rob Area, rob canceled!");
                    return;
                }
                API.sendNotificationToPlayer(playera, "You left the Rob Area, go back, otherwise the Rob will be canceled!");
                LeftAreaCounter++;
            }
            else
            {
                progress++;
                if (LeftAreaCounter >= 1)
                {
                    LeftAreaCounter--;
                }
                if (progress == 6 | progress == 12 | progress == 18 | progress == 24 | progress == 30 | progress == 36 | progress == 42 | progress == 48 | progress == 54)
                {
                    var instance = new UserFunctions();
                    instance.GivePlayerPoints(playera, 5);

                    int totalprofitper6 = totalprofit / 10;
                    int money = API.getEntityData(playera, "money") + totalprofitper6;
                    API.setEntityData(playera, "money", money);
                    int remain = 60 - progress;
                    API.sendNotificationToPlayer(playera, "~g~+ " + totalprofitper6 + "$ ~y~Remaining Time: " + remain + "seconds!");
                    API.triggerClientEvent(playera, "update_money_display", API.getEntityData(playera, "money"));
                }
            }

        }
    }
}
