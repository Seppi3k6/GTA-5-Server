﻿using System;
using System.Data;
using GTANetworkServer;

namespace Server
{
    public class DatabaseSync : Script
    {
        //load
        public void GetPlayerDatabaseStats(Client player)
        {
            string query = "SELECT * FROM users WHERE username='" + player.name + "'";
            DataTable result = API.exported.database.executeQueryWithResult(query);
            string adminlvldata = result.Rows[0][3].ToString();
            int playeradminlvldata = Int32.Parse(adminlvldata);
            string money = result.Rows[0][4].ToString();
            int playermoney = Int32.Parse(money);
            string wantedlevel = result.Rows[0][5].ToString();
            int playerwantedlevel = Int32.Parse(wantedlevel);
            API.setEntityData(player, "Adminlevel", playeradminlvldata);
            API.setEntityData(player, "money", playermoney);
            API.setEntityData(player, "wantedlevel", playerwantedlevel);
            API.setPlayerWantedLevel(player, playerwantedlevel);

            player.setSyncedData("money", API.getEntityData(player, "money"));
            API.triggerClientEvent(player, "update_money_display", API.getEntityData(player, "money"));

            var instance = new UserFunctions();
            instance.UpdatePlayerLevel(player);


            return;
        }

        //save
        public void SetPlayerDatabaseStats(Client player)
        {
            string query = "UPDATE users SET adminlevel='"+ API.getEntityData(player, "Adminlevel") + "', money='"+ API.getEntityData(player, "money") + "', wantedlevel='" + API.getEntityData(player, "wantedlevel") + "' WHERE username='" + player.name +"'";
            API.exported.database.executeQuery(query);
            API.consoleOutput("[SavePlayer]" + player.name + " Daten wurden gespeichert.");
            return;
        }
    }
}
