﻿using System;
using System.Data;
using GTANetworkServer;
using GTANetworkShared;
using System.Collections.Generic;


namespace Server
{
    public class ChatMessageFunction : Script
    {
        public void SendCopMessage(string message)
        {
            List<Client> AllPlayers = API.getAllPlayers();
            foreach (Client player in AllPlayers)
            {
                if (API.getEntityData(player, "fraction") == 1)
                {
                    API.sendChatMessageToPlayer(player, message);
                }
            }
            return;
        }
        public void SendCriminalMessage(string message)
        {
            List<Client> AllPlayers = API.getAllPlayers();
            foreach (Client player in AllPlayers)
            {
                if (API.getEntityData(player, "fraction") == 2)
                {
                    API.sendChatMessageToPlayer(player, message);
                }
            }
            return;
        }
        public void SendBigearsMessage(string message)
        {
            var instance = new UserFunctions();
            List<Client> AllPlayers = API.getAllPlayers();
            foreach (Client player in AllPlayers)
            {
                if (instance.GetPlayerBigears(player) == 1)
                {
                    API.sendChatMessageToPlayer(player, "[BIGEARS] " + message);
                }
            }
            return;
        }
    }
}
