﻿using System;
using System.Data;
using GTANetworkServer;
using GTANetworkShared;
using System.Collections.Generic;

namespace Server
{
    public class UserFunctions : Script
    {

        //Einstellungen________________________________________
        int NextLevelMultiplikator = 30;  //Sekunden
        //_____________________________________________________|


        public int GetPlayerAdminlevel(Client player)
        {
            return API.getEntityData(player, "Adminlevel");
        }
        public void GetPlayerAdminlevel(Client player, int newadminlevel)
        {
            API.setEntityData(player, "Adminlevel", newadminlevel);
            return;
        }


        public int GetPlayerBigears(Client player)
        {
            return API.getEntityData(player, "Bigears");
        }
        public void TogglePlayerBigears(Client player)
        {
            if(API.getEntityData(player, "Bigears") == 0)
            {
                API.setEntityData(player, "Bigears", 1);
                API.sendChatMessageToPlayer(player, "~g~Bigears Aktiviert, du kannst nun alles lesen.");
                return;
            }
            else if (API.getEntityData(player, "Bigears") == 1)
            {
                API.setEntityData(player, "Bigears", 0);
                API.sendChatMessageToPlayer(player, "~r~Bigears Deaktiviert.");
                return;
            }
        }
        public int GetPlayerFraction(Client player)
        {
            return API.getEntityData(player, "fraction");
        }
        public void SetPlayerFraction(Client player, int SetFraction)
        {
            API.setEntityData(player, "fraction", SetFraction);
            return;
        }


        public int GetPlayerPoints(Client player)
        {
            return API.getEntityData(player, "Points");
        }
        public void GivePlayerPoints(Client player, int GivePoints)
        {
            int currentPoints = 0;
            currentPoints = API.getEntityData(player, "Points");
            currentPoints += GivePoints;
            if(currentPoints >= GetPlayerLevel(player)*NextLevelMultiplikator)
            {
                API.setEntityData(player, "Points", 0);
                GivePlayerLevel(player, 1);
            }
            else
            {
                API.setEntityData(player, "Points", currentPoints);
            }
            UpdatePlayerLevel(player);
            return;
        }
        public void SetPlayerPoints(Client player, int SetPoints)
        {
            API.setEntityData(player, "Points", SetPoints);
            UpdatePlayerLevel(player);
            return;
        }
        public int GetPlayerLevel(Client player)
        {
            return API.getEntityData(player, "Level");
        }
        public void GivePlayerLevel(Client player, int GiveLevel)
        {
            int currentLevel = 0;
            currentLevel = API.getEntityData(player, "Level");
            currentLevel += GiveLevel;
            API.setEntityData(player, "Level", currentLevel);
            UpdatePlayerLevel(player);
            return;
        }
        public void SetPlayerLevel(Client player, int SetLevel)
        {
            API.setEntityData(player, "Level", SetLevel);
            UpdatePlayerLevel(player);
            return;
        }
        public int GetPlayerNextLevelPoints(Client player)
        {
            int pointstonextlevel = 0;
            pointstonextlevel = GetPlayerLevel(player)*NextLevelMultiplikator;
            return pointstonextlevel;
        }
        public void UpdatePlayerLevel(Client player)
        {
            API.triggerClientEvent(player, "update_LevelPoints_display", GetPlayerPoints(player), GetPlayerNextLevelPoints(player),GetPlayerLevel(player));
            return;
        }
    }
}
